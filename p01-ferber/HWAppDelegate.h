//
//  HWAppDelegate.h
//  p01-ferber
//
//  Created by Itai Ferber on 2/4/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@import UIKit;

@interface HWAppDelegate : UIResponder <UIApplicationDelegate>

#pragma mark - Properties
// The application's main window.
@property (strong, nonatomic) UIWindow *window;

@end

