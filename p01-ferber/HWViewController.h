//
//  HWViewController.h
//  p01-ferber
//
//  Created by Itai Ferber on 2/4/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

@import UIKit;
@import AVFoundation;

@interface HWViewController : UIViewController

#pragma mark - Properties
// The label used to communicate with the user.
@property (strong, nonatomic) IBOutlet UILabel *label;

// The button the user is supposed to press to get the label to change.
@property (strong, nonatomic) IBOutlet UIButton *initialButton;

// The number of times the user has pressed on the initial button.
@property NSUInteger initialTapCount;

// The button that will appear when the user tries to tap the first one.
@property (strong, nonatomic) IBOutlet UIButton *secondButton;

// Alternative buttons that show up after the secondary one.
@property (strong, nonatomic) IBOutlet UIButton *alternateButton1;
@property (strong, nonatomic) IBOutlet UIButton *alternateButton2;

// The button that will appear when the user is finally starting to get annoyed.
@property (strong, nonatomic) IBOutlet UIButton *forRealButton;

// The button to indicate user confirmation once all hope is lost.
@property (strong, nonatomic) IBOutlet UIButton *yesButton;

// The timer to use in animating the final label change.
@property (strong, nonatomic) NSTimer *dearLordFinallyTimer;

// The count to use in maintaining timer state.
@property NSUInteger timerCount;

// An audio player to play a sound with.
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

#pragma mark - Responding to User Actions
- (IBAction)initialButtonPress:(UIButton *)sender;
- (IBAction)secondButtonPress:(UIButton *)sender;
- (IBAction)alternateButtonPress:(UIButton *)sender;
- (IBAction)forRealButtonPress:(UIButton *)sender;
- (IBAction)yesButtonPress:(UIButton *)sender;

@end
