//
//  HWViewController.m
//  p01-ferber
//
//  Created by Itai Ferber on 2/4/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

#import "HWViewController.h"

@implementation HWViewController

#pragma mark - Responding to User Actions
- (IBAction)initialButtonPress:(UIButton *)sender
{
    switch (self.initialTapCount) {
        case 0:
        {
            self.label.text = @"You did it wrong.";
            break;
        }

        case 1:
        {
            self.label.text = @"It was stuck. Try again.";
            break;
        }

        case 2:
        {
            self.label.text = @"One last time, maybe?";
            break;
        }

        default:
        {
            self.label.text = @"Haha, psych!";
            self.initialButton.hidden = YES;
            self.secondButton.hidden = NO;
        }
    }
    
    self.initialTapCount += 1;
}

- (IBAction)secondButtonPress:(UIButton *)sender
{
    self.label.text = @"Which one?";
    self.secondButton.hidden = YES;
    self.alternateButton1.hidden = NO;
    self.alternateButton2.hidden = NO;
}

- (IBAction)alternateButtonPress:(UIButton *)sender
{
    UIButton *otherButton = sender == self.alternateButton1 ?
                                      self.alternateButton2 :
                                      self.alternateButton1;

    if (otherButton.hidden) {
        self.label.text = @"My, aren't you persistent?";
        self.forRealButton.hidden = NO;
    } else {
        self.label.text = @"No, the other one.";
    }

    sender.hidden = YES;
}

- (IBAction)forRealButtonPress:(UIButton *)sender
{
    self.label.text = @"Are you sure you want this?";
    self.forRealButton.hidden = YES;
    self.yesButton.hidden = NO;
}

- (IBAction)yesButtonPress:(UIButton *)sender
{
    self.label.text = @"Fine, you asked for it.";
    self.yesButton.hidden = YES;

    self.timerCount = 3;
    self.dearLordFinallyTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
}

- (void)timerTick:(NSTimer *)timer
{
    switch (self.timerCount) {
        case 0:
        {
            NSError *error = nil;
            NSURL *soundURL = [[NSBundle mainBundle] URLForResource:@"Hooray" withExtension:@"aif"];
            self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:&error];
            if (self.audioPlayer) {
                [self.audioPlayer play];
            } else {
                NSLog(@"Error in initializing audio player: %@", error);
            }

            self.label.text = @"Itai Ferber";
            [timer invalidate];
            break;
        }

        default:
        {
            self.label.text = [NSString stringWithFormat:@"%lu...", self.timerCount];
            break;
        }
    }

    self.timerCount -= 1;
}

@end
