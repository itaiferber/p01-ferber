//
//  main.m
//  p01-ferber
//
//  Created by Itai Ferber on 2/4/16.
//  Copyright © 2016 iferber1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HWAppDelegate class]));
    }
}
